package CROWN.Scheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ComEndPointSch {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public void startScheduleTask() {

        final ScheduledFuture<?> taskHandle = scheduler.scheduleAtFixedRate(
                new Runnable() {
                    public void run() {
                        try {
                            //StartComEndPoint();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                }, 0, 9, TimeUnit.HOURS);
    }

    public static void main(String[] args) {
        ComEndPointSch scheduler = new ComEndPointSch();
        scheduler.startScheduleTask();
    }
}