package CROWN.utility;

import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;

import java.io.IOException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.AssertJUnit.assertEquals;

public class ServiceTest {

    public static int RESPONSE_STATUS_CODE_200 = 200;
    public static int RESPONSE_STATUS_CODE_500 = 500;
    public static int RESPONSE_STATUS_CODE_400 = 400;
    public static int RESPONSE_STATUS_CODE_401 = 401;
    public static int RESPONSE_STATUS_CODE_201 = 201;

    public static void AssertHeaders(Headers Header, String Expected) throws IOException, InterruptedException {
        Assert.assertTrue(Header.toString().contains((Expected)));
    }

    public static void AssertCookies(Map<String, String> cookies, String Expected) throws IOException, InterruptedException {
        Assert.assertTrue(cookies.toString().contains((Expected)));
    }

    public static void AssertStatusCode(int ActualStatusCode, int ExpectedStatusCode) throws IOException, InterruptedException {
        assertEquals(ActualStatusCode, ExpectedStatusCode);
    }

    public static void AssertStatusLine(String ActualStatusLine) throws IOException, InterruptedException {
        assertEquals(ActualStatusLine, "HTTP/1.1 200 OK");
    }

    public static void AssertResponseBody(String jsonString, String Path, String Expected) throws Exception {
        assertThat(JsonPath.from(String.valueOf(jsonString)).get(Path), equalTo(Expected));
    }
}
