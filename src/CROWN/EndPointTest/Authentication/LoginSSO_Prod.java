package CROWN.EndPointTest.Authentication;

import CROWN.utility.Utility;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Assertions;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LoginSSO_Prod {

	public static String GetAuthToken(String tenantID, String Email, String Password) throws IOException, InterruptedException {

		tenantID = Utility.fetchService("tenantID_Text").toString();
		Email = Utility.fetchService("email_Text").toString();
		Password = Utility.fetchService("password_Text").toString();

		RestAssured.baseURI = Utility.fetchService("loginSSO_service").toString();

		RequestSpecification request = RestAssured.given();

		JSONObject requestParameter = new JSONObject();

		requestParameter.put("tenantId", tenantID);
		requestParameter.put("username", Email);
		requestParameter.put("password", Password);


		request.header("Content-Type", "application/json");

		RestAssuredConfig config = RestAssured.config()
				.httpClient(HttpClientConfig.httpClientConfig()
						.setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
						.setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
						.setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

		Response responseFromGenerateToken = request.config(config).body(requestParameter).config(config).post("/");

		String jsonString1 = responseFromGenerateToken.getBody().asString();

		String tokenGenerated = JsonPath.from(jsonString1).get("token");

		assertNotNull(tokenGenerated);

		String Pathre = JsonPath.from(jsonString1).get("user.email");
		Assertions.assertEquals(Email, Pathre);

		//System.out.println("\n" + "**************New Token Generated****************" + tokenGenerated + "**********New Token Generated**************");
		return tokenGenerated;
	}
}