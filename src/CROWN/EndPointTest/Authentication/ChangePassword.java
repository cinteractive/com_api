package CROWN.EndPointTest.Authentication;

import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

public class ChangePassword {

    @BeforeTest
    public Response Request() throws IOException, InterruptedException {

        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        JSONObject requestParameter = new JSONObject();

        requestParameter.put("newPassword", "@Ecomax1759");
        requestParameter.put("oldPassword", "");
        requestParameter.put("confirmPassword", "@Ecomax1759");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.config(config).body(requestParameter).put(Utility.fetchService("ChangePassword_service").toString());
    }

    @Description("Change Password")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void ChangePassword() throws Exception {
        //Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());
    }

    @Description("BadRequest ChangePassword")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void BadRequest_ChangePassword() throws Exception {

        Request().prettyPrint();
        String jsonStringg = Request().getBody().asString();
        int Pathre = JsonPath.from(jsonStringg).get("data.id");
        //assertThat(Pathre, is(instanceOf(Integer.class)));

    }
}
