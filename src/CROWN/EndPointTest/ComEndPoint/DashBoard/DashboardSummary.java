package CROWN.EndPointTest.ComEndPoint.DashBoard;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static CROWN.utility.ServiceTest.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DashboardSummary {

    @BeforeTest
    public Response Request() throws Exception {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        request.header("Cookie", "_csrf=7aea7c9515dfa1cefb47023f145bb410633c43449a30a2ef43f6368bd52fcd2fa%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22lp_Ks5g1w0tvlmsoZhcQ1EHc8_zKJ91K%22%3B%7D; com_se=uqn4c23q4p1ksm6v6rdsnndfeg");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.config(config).request(Method.GET, Utility.fetchService("DashBoardSummary_service").toString());
    }

    @Description("View Dashboard")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void ViewDashboard() throws Exception {


        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());

        String jsonStringg = Request().getBody().asString();
        String Pathre = JsonPath.from(jsonStringg).get("status");
        Assertions.assertEquals("success", Pathre);

        String Pathree = JsonPath.from(jsonStringg).get("message");
        Assertions.assertEquals("Request successful", Pathree);
    }

    @Description("Assert Dashboard Data")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void AssertDashboardData() throws Exception {

        String Pathre = Request().getBody().asString();

        Assertions.assertTrue(Pathre.contains("paid"));
        Assertions.assertTrue(Pathre.contains("amount"));
        Assertions.assertTrue(Pathre.contains("count"));
        Assertions.assertTrue(Pathre.contains("pending"));
        Assertions.assertTrue(Pathre.contains("total"));
        Assertions.assertTrue(Pathre.contains("graph"));
        Assertions.assertTrue(Pathre.contains("year"));
        Assertions.assertTrue(Pathre.contains("amount_format"));
        Assertions.assertTrue(Pathre.contains("pending_orders"));
        Assertions.assertTrue(Pathre.contains("sales_target_amount"));
        Assertions.assertTrue(Pathre.contains("sales_made_amount"));
    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Header_Cookies_Response() throws Exception {

        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        System.out.println(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }
}
