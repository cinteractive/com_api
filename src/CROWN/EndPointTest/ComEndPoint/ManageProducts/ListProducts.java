package CROWN.EndPointTest.ComEndPoint.ManageProducts;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.ServiceTest.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class ListProducts {

    @BeforeTest
    public Response Request() throws IOException, InterruptedException {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.config(config).request(Method.GET, Utility.fetchService("ListProduct_service").toString());
    }

    @Description("List Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void ListProduct() throws Exception {

        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());
        String jsonStringg = Request().getBody().asString();
        String Pathre = JsonPath.from(jsonStringg).get("status");
        Assertions.assertEquals("success", Pathre);

    }

    @Description("Assert Message")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void AssetMessage() throws Exception {

        String jsonStringg = Request().getBody().asString();
        String Pathre = JsonPath.from(jsonStringg).get("message");
        Assertions.assertEquals("Request successful", Pathre);
    }

    @Description("Assert Data")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void AssertData() throws Exception {

        String Pathre = Request().getBody().asString();

        Assertions.assertTrue(Pathre.contains("id"));
        Assertions.assertTrue(Pathre.contains("category"));
        Assertions.assertTrue(Pathre.contains("name"));
        Assertions.assertTrue(Pathre.contains("code"));
        Assertions.assertTrue(Pathre.contains("description"));
        Assertions.assertTrue(Pathre.contains("slug"));
        Assertions.assertTrue(Pathre.contains("image"));
        Assertions.assertTrue(Pathre.contains("price"));
        Assertions.assertTrue(Pathre.contains("quantity"));
        Assertions.assertTrue(Pathre.contains("validity"));
        Assertions.assertTrue(Pathre.contains("no_qty_limit"));
        Assertions.assertTrue(Pathre.contains("vat_option"));
        Assertions.assertTrue(Pathre.contains("has_vat"));
        Assertions.assertTrue(Pathre.contains("vat_amount"));
        Assertions.assertTrue(Pathre.contains("vat_percent"));
        Assertions.assertTrue(Pathre.contains("variations"));
    }

    @Description("Assert Data Count")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void AssertDataCount() throws Exception {

        String Pathre = Request().getBody().asString();

        Assertions.assertTrue(Pathre.contains("current_count"));
        Assertions.assertTrue(Pathre.contains("total_count"));
        Assertions.assertTrue(Pathre.contains("items_per_page"));
        Assertions.assertTrue(Pathre.contains("page_no"));
        Assertions.assertTrue(Pathre.contains("pages"));
    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void Header_Cookies_Response() throws Exception {

        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }
}
