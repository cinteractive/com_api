package CROWN.EndPointTest.ComEndPoint.ManageOrders;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static CROWN.utility.ServiceTest.*;
import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreateOrder {


    @BeforeTest
    public Response Request() throws IOException, InterruptedException {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        final Map<String, Object> requestParameter = new HashMap<>();

        requestParameter.put("customer_name", "Jenga Cross");
        requestParameter.put("customer_phone", 44444133);
        requestParameter.put("customer_email", "joecrosxxs@yopmail.com");
        requestParameter.put("products", asList(new HashMap<String, Object>() {{
            put("id", "1");
            put("quantity", 11);
        }}));
        requestParameter.put("delivery_type", "PICKUP");
        requestParameter.put("payment_mode", "ONLINE");
        requestParameter.put("discount_percent", 10);

        RestAssuredConfig config1 = RestAssured.config().httpClient(HttpClientConfig.httpClientConfig().setParam("CONNECTION_MANAGER_TIMEOUT", 3000));

        return request.body(requestParameter).config(config1).post(Utility.fetchService("OrderDetail_service").toString());
    }

    @BeforeTest
    public Response Requestless100() throws IOException, InterruptedException {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        final Map<String, Object> requestParameter = new HashMap<>();

        requestParameter.put("customer_name", "Jenga Cross");
        requestParameter.put("customer_phone", 44444133);
        requestParameter.put("customer_email", "joecrosxxs@yopmail.com");
        requestParameter.put("products", asList(new HashMap<String, Object>() {{
            put("id", "1");
            put("quantity", 1);
        }}));
        requestParameter.put("delivery_type", "PICKUP");
        requestParameter.put("payment_mode", "ONLINE");
        requestParameter.put("discount_percent", 10);

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.body(requestParameter).config(config).post(Utility.fetchService("OrderDetail_service").toString());
    }

    @Description("Assert Order Status")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void AssertOrderStatus() throws Exception {
        Assert.assertTrue(Request().then().extract().jsonPath().getString("status").contains("success"));
    }

    @Description("Assert Message")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void AssertMessage() throws Exception {
        Assert.assertTrue(Request().then().extract().jsonPath().getString("message").contains("Request successful"));
    }


    @Description("Assert Order ID")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void OrderID() throws Exception {
        Assert.assertTrue(Request().then().extract().jsonPath().getInt("data.id") > 0);
    }

    @Description("Assert Cicod Order ID")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void cicodOrderID() throws Exception {
        Assert.assertNotNull(Request().then().extract().jsonPath().getString("data.cicod_order_id"));
    }

    @Description("Assert Payment Link")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void PaymentLink() throws Exception {
        Assert.assertTrue(Request().then().extract().jsonPath().getString("data.payment_link").contains("api.ravepay"));
    }

    @Description("Assert Payment Less Than 100")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 6)
    public void AssertPaymentLessThan100() throws Exception {
        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Requestless100().getStatusCode());
        Assert.assertTrue(Requestless100().then().extract().jsonPath().getString("message").contains("Online payment amount cannot be less than NGN100."));
    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 7)
    public void Header_Cookies_Response() throws Exception {

        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }

}
