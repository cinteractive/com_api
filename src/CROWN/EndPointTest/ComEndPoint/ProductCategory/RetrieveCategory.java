package CROWN.EndPointTest.ComEndPoint.ProductCategory;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.ServiceTest.*;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RetrieveCategory {

    @BeforeTest
    public Response Request() throws IOException, InterruptedException {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.config(config).request(Method.GET, Utility.fetchService("RetrieveCategory_service").toString());
    }


    @Description("Retrieve Category")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void RetrieveCategory() throws Exception {

        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());

        String jsonStringg = Request().getBody().asString();
        String Pathre = JsonPath.from(jsonStringg).get("status");
        Assertions.assertEquals("success", Pathre);

        Assertions.assertTrue(jsonStringg.contains("id"));
        Assertions.assertTrue(jsonStringg.contains("name"));
        Assertions.assertTrue(jsonStringg.contains("slug"));
        Assertions.assertTrue(jsonStringg.contains("description"));
    }

    @Description("Assert Category Data")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void AssertCategoryData() throws Exception {

        String jsonStringg = Request().getBody().asString();

        int Pathre = JsonPath.from(jsonStringg).get("data.id");
        Assertions.assertEquals(7, Pathre);

        String Pathree = JsonPath.from(jsonStringg).get("data.name");
        assertThat(Pathree, is(instanceOf(String.class)));
    }

    @Description("Assert Product Data")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void AssertProductDimension() throws Exception {

        String jsonStringg = Request().getBody().asString();

        String Pathreee = JsonPath.from(jsonStringg).get("data.date_created");
        Assertions.assertNotNull(Pathreee);

        String Pathreeee = JsonPath.from(jsonStringg).get("data.date_updated");
        Assertions.assertNull(Pathreeee);
    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void Header_Cookies_Response() throws Exception {

        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }
}
