package CROWN.EndPointTest.ComEndPoint.ProductCategory;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Randomstuff;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.ServiceTest.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UpdateProductCategory {

    @BeforeMethod
    public Response Request() throws IOException, InterruptedException {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        JSONObject requestParameter = new JSONObject();

        requestParameter.put("name", Randomstuff.ListRandom());
        requestParameter.put("description", Randomstuff.ListRandom());
        requestParameter.put("on_webshop", 1);

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.config(config).body(requestParameter).put(Utility.fetchService("UpdateProductCategory_service").toString());
    }

    @Description("Update Product Category")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void UpdateCategory() throws Exception {
        Request().prettyPrint();
        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());

        String jsonStringg = Request().getBody().asString();
        String Pathre = JsonPath.from(jsonStringg).get("status");
        Assertions.assertEquals("success", Pathre);
    }

    @Description("Assert Body")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void AssertBody() throws Exception {
        String jsonStringg = Request().getBody().asString();
        int Pathre = JsonPath.from(jsonStringg).get("data.id");
        assertThat(Pathre, is(instanceOf(Integer.class)));
    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Header_Cookies_Response() throws Exception {

        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }
}
