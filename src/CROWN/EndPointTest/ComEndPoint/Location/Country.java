package CROWN.EndPointTest.ComEndPoint.Location;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static CROWN.utility.ServiceTest.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Country {

    @BeforeTest
    public Response Request() throws Exception {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.config(config).request(Method.GET, Utility.fetchService("Country_service").toString());
    }

    @Description("Country")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void Country() throws Exception {
        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());

        String jsonStringg = Request().getBody().asString();
        String Pathre = JsonPath.from(jsonStringg).get("status");
        Assertions.assertEquals("success", Pathre);
    }


    @Description("Assert Status")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void AssetStatus() throws Exception {
        String jsonStringg = Request().getBody().asString();
        String Pathre = JsonPath.from(jsonStringg).get("status");
        Assertions.assertEquals("success", Pathre);
    }

    @Description("Assert Message")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void AssetMessage() throws Exception {
        String jsonStringg = Request().getBody().asString();
        String Pathre = JsonPath.from(jsonStringg).get("message");
        Assertions.assertEquals("Request successful", Pathre);
    }

    @Description("Assert Data")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void AssertData() throws Exception {
        String Pathre = Request().getBody().asString();
        Assertions.assertTrue(Pathre.contains("id"));
        Assertions.assertTrue(Pathre.contains("name"));
        Assertions.assertTrue(Pathre.contains("is_active"));
    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 5)
    public void Header_Cookies_Response() throws Exception {

        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }
}
