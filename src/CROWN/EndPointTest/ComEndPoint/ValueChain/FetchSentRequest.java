package CROWN.EndPointTest.ComEndPoint.ValueChain;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.params.CoreConnectionPNames;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import static CROWN.utility.ServiceTest.RESPONSE_STATUS_CODE_200;

public class FetchSentRequest {

    @Description("Fetch Sent Request")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void FetchAllSentRequest() throws Exception {

        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam(CoreConnectionPNames.SO_TIMEOUT, Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        Response ProductResponse = request.config(config).request(Method.GET, Utility.fetchService("FetchedSentRequest_service").toString());

        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, ProductResponse.getStatusCode());
    }

    @Description("Assert Filter Approved")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void FetchAllSentRequest_AssertFilterApproved() throws Exception {

        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam(CoreConnectionPNames.SO_TIMEOUT, Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        Response ProductResponse = request.config(config).queryParam("filter[status]", "APPROVED").request(Method.GET, Utility.fetchService("FetchedSentRequest_service").toString());

        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, ProductResponse.getStatusCode());

        String Pathre = ProductResponse.getBody().asString();

        ProductResponse.prettyPrint();

        Assertions.assertTrue(Pathre.contains("id"));
        Assertions.assertTrue(Pathre.contains("seller_id"));
    }

    @Description("Assert Filter Pending")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void FetchAllSentRequest_AssertFilterPending() throws Exception {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        Response ProductResponse = request.config(config).queryParam("filter[status]", "PENDING").request(Method.GET, Utility.fetchService("FetchedSentRequest_service").toString());

        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, ProductResponse.getStatusCode());

        String Pathre = ProductResponse.getBody().asString();

        ProductResponse.prettyPrint();
        Assertions.assertTrue(Pathre.contains("id"));
        Assertions.assertTrue(Pathre.contains("seller_id"));
    }

    @Description("Assert Filter Rejected")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void FetchAllSentRequest_AssertFilterRejected() throws Exception {

        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        Response ProductResponse = request.config(config).queryParam("filter[status]", "REJECTED").request(Method.GET, Utility.fetchService("FetchedSentRequest_service").toString());

        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, ProductResponse.getStatusCode());

        ProductResponse.prettyPrint();
    }

}
