package CROWN.EndPointTest.ComEndPoint.ValueChain;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.ServiceTest.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ViewSellerOrder {

    @BeforeTest
    public Response Request() throws IOException, InterruptedException {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.config(config).queryParam("id", 10545).queryParam("orderId", 1611955).request(Method.GET, Utility.fetchService("ViewSellersOrder_service").toString());
    }

    @Description("Retrieve Product")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void RetrieveProduct() throws Exception {
        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());
    }

    @Description("Assert Product Data")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void AssertProductData() throws Exception {

        String jsonStringg = Request().getBody().asString();

        Assertions.assertTrue(jsonStringg.contains("token"));
        Assertions.assertTrue(jsonStringg.contains("created_by_merchant_id"));
        Assertions.assertTrue(jsonStringg.contains("order_id_src"));
        Assertions.assertTrue(jsonStringg.contains("ticket_id"));
        Assertions.assertTrue(jsonStringg.contains("invoice_no"));
        Assertions.assertTrue(jsonStringg.contains("customer_id"));
        Assertions.assertTrue(jsonStringg.contains("payment_link"));

        Boolean Pathree = JsonPath.from(jsonStringg).get("success");
        Assertions.assertTrue(Pathree);
    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Header_Cookies_Response() throws Exception {
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }
}
