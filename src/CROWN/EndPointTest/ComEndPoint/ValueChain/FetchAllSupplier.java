package CROWN.EndPointTest.ComEndPoint.ValueChain;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;

import static CROWN.utility.ServiceTest.*;
import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class FetchAllSupplier {

    @BeforeTest
    public Response Request() throws IOException, InterruptedException {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        JSONObject requestParameter = new JSONObject();

        requestParameter.put("products", asList(new HashMap<String, Object>() {{
            put("Products", 100);
        }}));

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.body(requestParameter).config(config).queryParam("id", "10545").post(Utility.fetchService("ApproveBuyer_service").toString());
    }

    @Description("Fetch All Supplier")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void FetchAllSupplier() throws Exception {
        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());
        Request().prettyPrint();

        String jsonStringg = Request().getBody().asString();
        Boolean Pathre = JsonPath.from(jsonStringg).get("success");
        Assertions.assertTrue(Pathre);
    }

    @Description("Assert Data")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void FetchAllSupplierAssertData() throws Exception {

        String Pathre = Request().getBody().asString();
/*
        Assertions.assertTrue(Pathre.contains("id"));
        Assertions.assertTrue(Pathre.contains("request_id"));
        Assertions.assertTrue(Pathre.contains("buyer_id"));
        Assertions.assertTrue(Pathre.contains("seller_id"));
        Assertions.assertTrue(Pathre.contains("seller_name"));
        Assertions.assertTrue(Pathre.contains("approved_by"));
        Assertions.assertTrue(Pathre.contains("product_categories"));
        Assertions.assertTrue(Pathre.contains("no_of_orders"));
        Assertions.assertTrue(Pathre.contains("no_of_products"));
        Assertions.assertTrue(Pathre.contains("value_of_orders"));
        Assertions.assertTrue(Pathre.contains("create_time"));
        Assertions.assertTrue(Pathre.contains("is_active"));
        Assertions.assertTrue(Pathre.contains("minimum_spend"));
        Assertions.assertTrue(Pathre.contains("buyer_stock_is_visible"));
 */

    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Header_Cookies_Response() throws Exception {
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }
}
