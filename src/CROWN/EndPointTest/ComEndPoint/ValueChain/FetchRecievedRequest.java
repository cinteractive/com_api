package CROWN.EndPointTest.ComEndPoint.ValueChain;

import CROWN.EndPointTest.Authentication.LoginSSO_Prod;
import CROWN.utility.Utility;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.params.CoreConnectionPNames;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Description;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.ServiceTest.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class FetchRecievedRequest {

    @BeforeTest
    public Response Request() throws IOException, InterruptedException {

        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        request.header("Authorization", "Bearer " + token).header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam("http.connection.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.socket.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam("http.connection-manager.timeout", Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        return request.config(config).request(Method.GET, Utility.fetchService("FetchRecievedRequest_service").toString());
    }

    @Description("Fetch Recieved Request")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 1)
    public void FetchRecievedRequest() throws Exception {
        Request().prettyPrint();
        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());
    }

    @Description("Assert Filter Approved")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 2)
    public void AssertFilterApproved() throws Exception {
        String Pathre = Request().getBody().asString();
        Request().prettyPrint();
        Assertions.assertTrue(Pathre.contains("id"));
        Assertions.assertTrue(Pathre.contains("seller_id"));
    }

    @Description("Assert Filter Pending")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void FetchRecievedRequest_AssertFilterPending() throws Exception {

        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, Request().getStatusCode());

        String Pathre = Request().getBody().asString();
        Assertions.assertTrue(Pathre.contains("id"));
        Assertions.assertTrue(Pathre.contains("seller_id"));
    }

    @Description("Assert Filter Rejected")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 4)
    public void FetchRecievedRequest_AssertFilterRejected() throws Exception {
        String token = LoginSSO_Prod.GetAuthToken("tenantID_Text", "email_Text", "password_Text");
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        RestAssuredConfig config = RestAssured.config()
                .httpClient(HttpClientConfig.httpClientConfig()
                        .setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, Integer.parseInt((String) Utility.fetchService("Service_timeout")))
                        .setParam(CoreConnectionPNames.SO_TIMEOUT, Integer.parseInt((String) Utility.fetchService("Service_timeout"))));

        Response ProductResponse = request.config(config).queryParam("filter[status]", "REJECTED").request(Method.GET, Utility.fetchService("FetchRecievedRequest_service").toString());
        ProductResponse.prettyPrint();

        Assertions.assertEquals(RESPONSE_STATUS_CODE_200, ProductResponse.getStatusCode());
    }

    @Description("Headers and Cookies Response")
    @Severity(SeverityLevel.CRITICAL)
    @Test(priority = 3)
    public void Header_Cookies_Response() throws Exception {

        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Xframe_TEXT"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("AccessControl_Text"));
        AssertHeaders(Request().getHeaders(), (String) Utility.fetchService("Cache-Control_Text"));
        AssertCookies(Request().getCookies(), (String) Utility.fetchService("com_se"));
        assertNotNull(Request().getCookies());
        AssertStatusLine(Request().statusLine());
    }
}
